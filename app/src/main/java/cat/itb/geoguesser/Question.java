package cat.itb.geoguesser;

public class Question
{
    private String question;
    private Answer answer;
    private int questionImgRef;

    public Question(String question, Answer answer, int questionImgRef)
    {
        this.question = question;
        this.answer = answer;
        this.questionImgRef = questionImgRef;
    }

    public String getQuestion() {
        return question;
    }

    public Answer getAnswer() {
        return answer;
    }

    public int getQuestionImgRef() {
        return questionImgRef;
    }
}
