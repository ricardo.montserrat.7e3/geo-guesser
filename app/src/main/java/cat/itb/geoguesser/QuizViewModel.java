package cat.itb.geoguesser;

import android.view.View;

import androidx.lifecycle.ViewModel;

import java.util.Arrays;
import java.util.List;

public class QuizViewModel extends ViewModel
{
    private List<Answer> possibleAnswers = Arrays.asList(
            new Answer("Spain", Answer.AnswerType.Country),
            new Answer("France", Answer.AnswerType.Country),
            new Answer("England", Answer.AnswerType.Country),
            new Answer("Portugal", Answer.AnswerType.Country),
            new Answer("Scotland", Answer.AnswerType.Country),
            new Answer("China", Answer.AnswerType.Country),
            new Answer("Japan", Answer.AnswerType.Country),
            new Answer("Taiwan", Answer.AnswerType.Country),
            new Answer("Ecuador", Answer.AnswerType.Country),
            new Answer("Shark", Answer.AnswerType.Animal),
            new Answer("Beaver", Answer.AnswerType.Animal),
            new Answer("Kiwi", Answer.AnswerType.Animal),
            new Answer("Crow", Answer.AnswerType.Animal),
            new Answer("Lion", Answer.AnswerType.Animal),
            new Answer("Phoenix", Answer.AnswerType.Animal),
            new Answer("Penguin", Answer.AnswerType.Animal),
            new Answer("Iguana", Answer.AnswerType.Animal),
            new Answer("Turtle", Answer.AnswerType.Animal)
    );

    private List<Question> questions = Arrays.asList(
            new Question("Did you know Kangaroos can't walk backwards, where are they from?", new Answer("Australia", Answer.AnswerType.Country), R.drawable.kangaroo),
            new Question("What's the name of this New Zelandan Bird?", new Answer("Kiwi", Answer.AnswerType.Animal), R.drawable.kiwi),
            new Question("This animal is originary from Spain, is called...", new Answer("Bull", Answer.AnswerType.Animal), R.drawable.bull),
            new Question("Did you know that the cow is a sacred animal, but where?", new Answer("India", Answer.AnswerType.Country), R.drawable.cow),
            new Question("Lemurs, live in the same place as the movie...", new Answer("Madagascar", Answer.AnswerType.Country), R.drawable.lemurs),
            new Question("Originary from China, known as a symbol of peace for them the famous...", new Answer("Panda", Answer.AnswerType.Animal), R.drawable.panda),
            new Question("The Beaver, loves wood and hardwork, where do you think he is from?", new Answer("Canada", Answer.AnswerType.Country), R.drawable.beaver),
            new Question("When you see this animal you can only think of one place...", new Answer("USA", Answer.AnswerType.Country), R.drawable.bald_eagle),
            new Question("Is hard to get to them, and they can't fly, they are...", new Answer("Inaccessible Island rail", Answer.AnswerType.Animal), R.drawable.innac_island_rail),
            new Question("Easy to differentiate them between their other brothers at the antartic!", new Answer("Penguin Emperor", Answer.AnswerType.Animal), R.drawable.penguin_emperor)
    );
    private int pointer = -1;

    private final int hints = 3;
    private int hintsLeft = 3;

    private float userScore = 0;

    private final int maxSecondsTimer = 10;
    private int secondsTimer = 10;

    private int correctLayoutVisibility = View.GONE;
    private int incorrectLayoutVisibility = View.GONE;

    private boolean isShowingEndScreen = false;

    public List<Answer> getPossibleAnswers() { return possibleAnswers; }

    public List<Question> getQuestions() { return questions; }

    public int getHints() { return hints; }

    public int getHintsLeft() { return hintsLeft; }

    public float getUserScore() { return userScore; }

    public int getSecondsTimer() { return secondsTimer; }

    public void setHintsLeft(int hintsLeft) { this.hintsLeft = hintsLeft; }

    public void setUserScore(float userScore) { this.userScore = userScore; }

    public void setSecondsTimer(int secondsTimer) { this.secondsTimer = secondsTimer; }

    public int getPointer() { return pointer; }

    public void setPointer(int pointer) { this.pointer = pointer; }

    public void setQuestions(List<Question> questions) { this.questions = questions; }

    public void setPossibleAnswers(List<Answer> possibleAnswers) { this.possibleAnswers = possibleAnswers; }

    public int getMaxSecondsTimer() { return maxSecondsTimer; }

    public int getCorrectLayoutVisibility() { return correctLayoutVisibility; }

    public void setCorrectLayoutVisibility(int correctLayoutVisibility) { this.correctLayoutVisibility = correctLayoutVisibility; }

    public int getIncorrectLayoutVisibility() { return incorrectLayoutVisibility; }

    public void setIncorrectLayoutVisibility(int incorrectLayoutVisibility) { this.incorrectLayoutVisibility = incorrectLayoutVisibility; }

    public boolean isShowingEndScreen() { return isShowingEndScreen; }

    public void setShowingEndScreen(boolean showingEndScreen) { isShowingEndScreen = showingEndScreen; }
}
