package cat.itb.geoguesser;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private QuizViewModel viewModel;

    private List<Answer> possibleAnswers;

    private List<Question> questions;

    private TextView questionText;
    private TextView questionNumberText;
    private ImageView questionImage;
    private ProgressBar timerBar;

    private View correctLayout;
    private View incorrectLayout;

    private List<Button> buttons;

    private int pointer;

    private int hintsLeft;

    private float userScore;

    private Button hintButton;

    private CountDownTimer actualTimer;
    private int secondsTimer;

    private Answer getAnswerDifferentFrom(Answer otherAnswer)
    {
        for (Answer answer : possibleAnswers)
        {
            if (answer.getAnswerType() == otherAnswer.getAnswerType() && !answer.getAnswer().equalsIgnoreCase(otherAnswer.getAnswer()))
            {
                Collections.shuffle(possibleAnswers);
                viewModel.setPossibleAnswers(possibleAnswers);
                return answer;
            }
        }
        return null;
    }

    private void startTimer()
    {
        timerBar.setProgress(secondsTimer);
        viewModel.setSecondsTimer(secondsTimer);
        if (actualTimer != null)
        {
            actualTimer.cancel();
            secondsTimer = viewModel.getMaxSecondsTimer();
        }
        actualTimer = new CountDownTimer(secondsTimer * 1000, 1000)
        {
            public void onTick(long millisUntilFinished)
            {
                timerBar.setProgress(secondsTimer);
                secondsTimer--;
                viewModel.setSecondsTimer(secondsTimer);
            }

            public void onFinish()
            {
                secondsTimer = viewModel.getMaxSecondsTimer();
                loadNextQuestion();
                startTimer();
            }
        }.start();
    }

    private void loadQuestion()
    {
        Question nextQuestion = questions.get(pointer);
        String nextQuestionText = "Question " + (pointer + 1) + " out of " + questions.size();

        questionText.setText(nextQuestion.getQuestion());
        questionNumberText.setText(nextQuestionText);
        questionImage.setImageResource(nextQuestion.getQuestionImgRef());

        Answer tempAnswer = nextQuestion.getAnswer();
        Collections.shuffle(buttons);
        for (Button button : buttons)
        {
            if (tempAnswer != null) button.setText(tempAnswer.getAnswer());
            tempAnswer = getAnswerDifferentFrom(tempAnswer);
        }
    }

    private void showEndMessage()
    {
        viewModel.setShowingEndScreen(true);
        new AlertDialog.Builder(this).setTitle("Your Final Score Is...").setPositiveButton("Play Again!", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                correctLayout.setVisibility(View.GONE);
                incorrectLayout.setVisibility(View.GONE);
                viewModel.setCorrectLayoutVisibility(View.GONE);
                viewModel.setIncorrectLayoutVisibility(View.GONE);
                loadNextQuestion();
                startTimer();
                viewModel.setShowingEndScreen(false);
                hintsLeft = viewModel.getHints();
                userScore = 0;
                Collections.shuffle(questions);
                viewModel.setQuestions(questions);
                viewModel.setHintsLeft(hintsLeft);
                viewModel.setUserScore(userScore);
            }
        }).setMessage(Math.round(userScore * 100 / questions.size()) + " out of 100").show();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.correctLayout:
            case R.id.incorrectLayout:
                v.setVisibility(View.GONE);
                viewModel.setIncorrectLayoutVisibility(View.GONE);
                viewModel.setCorrectLayoutVisibility(View.GONE);
                startTimer();
                break;
            case R.id.hintButton:
                Toast.makeText(this, questions.get(pointer).getAnswer().getAnswer(), Toast.LENGTH_SHORT).show();
                hintsLeft--;
                if (hintsLeft < 1) hintButton.setEnabled(false);
                viewModel.setHintsLeft(hintsLeft);
                break;
            default:
                if (((Button) v).getText().toString().equalsIgnoreCase(questions.get(pointer).getAnswer().getAnswer()))
                {
                    userScore++;
                    correctLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    if (userScore > 0) userScore -= 0.5;
                    incorrectLayout.setVisibility(View.VISIBLE);
                }
                viewModel.setIncorrectLayoutVisibility(incorrectLayout.getVisibility());
                viewModel.setCorrectLayoutVisibility(correctLayout.getVisibility());
                viewModel.setUserScore(userScore);
                if (pointer == questions.size() - 1)
                {
                    actualTimer.cancel();
                    showEndMessage();
                }
                else loadNextQuestion();
                break;
        }
    }

    private void setViewModel()
    {
        viewModel = new ViewModelProvider(this).get(QuizViewModel.class);

        questions = viewModel.getQuestions();
        pointer = viewModel.getPointer();

        timerBar.setMax(viewModel.getMaxSecondsTimer());

        possibleAnswers = viewModel.getPossibleAnswers();

        hintsLeft = viewModel.getHintsLeft();

        userScore = viewModel.getUserScore();

        secondsTimer = viewModel.getSecondsTimer();
        timerBar.setProgress(secondsTimer);

        incorrectLayout.setVisibility(viewModel.getIncorrectLayoutVisibility());
        correctLayout.setVisibility(viewModel.getCorrectLayoutVisibility());

        if (pointer == -1)
        {
            Collections.shuffle(questions);
            loadNextQuestion();
            startTimer();
        }
        else if(viewModel.isShowingEndScreen())
        {
            showEndMessage();
        }
        else
        {
            loadQuestion();
            startTimer();
        }
    }

    private void loadNextQuestion()
    {
        pointer = (pointer + 1) % questions.size();
        viewModel.setPointer(pointer);
        loadQuestion();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questionText = findViewById(R.id.questionText);
        questionNumberText = findViewById(R.id.questionNumberText);
        questionImage = findViewById(R.id.questionImg);
        timerBar = findViewById(R.id.timerBar);

        incorrectLayout = findViewById(R.id.incorrectLayout);
        correctLayout = findViewById(R.id.correctLayout);

        buttons = Arrays.asList(
                (Button) findViewById(R.id.buttonAnswer1),
                (Button) findViewById(R.id.buttonAnswer2),
                (Button) findViewById(R.id.buttonAnswer3),
                (Button) findViewById(R.id.buttonAnswer4)
        );

        for (Button button : buttons) { button.setOnClickListener(this); }

        incorrectLayout.setOnClickListener(this);
        correctLayout.setOnClickListener(this);

        hintButton = findViewById(R.id.hintButton);

        hintButton.setOnClickListener(this);

        setViewModel();
    }
}