package cat.itb.geoguesser;

public class Answer
{
    private String answer;
    private AnswerType answerType;
    public enum AnswerType {Animal, Country}

    public Answer()
    {

    }

    public Answer(String answer, AnswerType answerType)
    {
        this.answer = answer;
        this.answerType = answerType;
    }

    public String getAnswer() {
        return answer;
    }

    public AnswerType getAnswerType() {
        return answerType;
    }
}
